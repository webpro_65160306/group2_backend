/* eslint-disable prettier/prettier */
import { IsNotEmpty } from 'class-validator';
export class CreateMaterialDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  quantityLasts: number;
  @IsNotEmpty()
  quantityNow: number;
  @IsNotEmpty()
  quantityIm: number;
  @IsNotEmpty()
  MinQuantity: number;
  @IsNotEmpty()
  unit: string;
  @IsNotEmpty()
  price: number;
}
