/* eslint-disable prettier/prettier */
import { PartialType } from '@nestjs/swagger';
import { CreateMaterialDto } from './create-material.dao';

export class UpdateMaterialDto extends PartialType(CreateMaterialDto) {}
