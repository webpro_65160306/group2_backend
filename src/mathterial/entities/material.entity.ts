/* eslint-disable prettier/prettier */

import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  quantityLasts: number;
  @Column()
  quantityNow: number;
  @Column()
  quantityIm: number;
  @Column()
  MinQuantity: number;
  @Column()
  unit: string;
  @Column()
  price: number;
}
